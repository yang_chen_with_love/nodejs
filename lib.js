const yaml = require('js-yaml');
const path = require("path");
const fs   = require("fs");
const prodDir = `${__dirname}/prod`;
const devDir = `${__dirname}/dev`;
const qaDir = `${__dirname}/qa`;
const files  = [];

function getAllFiles(dir) {
    fs.readdirSync(dir).forEach(file => {
        const absoluteDir = path.join(dir, file);
        if (fs.statSync(absoluteDir).isDirectory()) return getAllFiles(absoluteDir);
        else return files.push(absoluteDir);
    });
}

function converYamlToJson(files){
    const jsonFile = {}
    for (const file of files) {
        const fileContent = yaml.load(fs.readFileSync(file))
        const domainDetails = fileContent['domains']
        jsonFile[domainDetails.domain] = domainDetails
        for (const key in domainDetails) {
            if (key==='domain') {
              delete jsonFile[domainDetails.domain][key];
            }
          }
    }
    return jsonFile
}

async function generateJsonFile() {
    getAllFiles(prodDir);
    getAllFiles(devDir);
    getAllFiles(qaDir);
    const jsonData = converYamlToJson(files)
    return jsonData
}

module.exports = { generateJsonFile }
