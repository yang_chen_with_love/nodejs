const { generateJsonFile } = require('./lib');
const data = generateJsonFile();

function loadDomains(platform) {
    const domainMapping = [];
    for (const [domainName, domainDetails] of Object.entries(data)) {
      if (domainDetails.env === platform) {
        const document = {
          domain: domainName,
          country: domainDetails.countryCode,
          timeZone: domainDetails.timezone,
        };
        domainMapping.push(document);
      }
    }
    console.log(domainMapping);
}

loadDomains('qa')
