# Nodejs

## 1.development

Build a tool to monitor the error logs of the Ubuntu system

log file: 
```
/var/log/syslog
/var/log/kern.log
```

requirements:
    - monitor the 2 log files and capture the errors.
    - if any error, send notification to email address: `dummy_email@coatsgroup.com`
    - if the same erorr keeps happening in 24 hours, do not send the notification twice.




## 2.deploy

Create a CI to deploy the tool to a AWS EC2 instance.
(use fake EC2 IP and fake access keys are fine, no need to actually deploy)





## 3.debug
there is a bug in the project, please debug and fix it.

exceptation:
when run blow command
```
node index.js
```

when the argument is `prod` (line 19 in the index.js file: loadDomains('prod')), output:
```
[
  { domain: 'china.prod.io', country: 'cn', timeZone: 'Asia/Shanghai' },
  { domain: 'jp.prod.io', country: 'jp', timeZone: 'Asia/tokyo' },
  { domain: 'uk.prod.io', country: 'uk', timeZone: 'Europe/Lodon' },
  { domain: 'fr.prod.io', country: 'fr', timeZone: 'Europe/Paris' },
  { domain: 'dubai.prod.io', country: 'ae', timeZone: 'Asia/Dubai' }
]
```

when the argument is `dev` (line 19 in the index.js file: loadDomains('dev')), output:
```
[
  { domain: 'china.dev.io', country: 'cn', timeZone: 'Asia/Shanghai' },
  { domain: 'jp.dev.io', country: 'jp', timeZone: 'Asia/tokyo' },
  { domain: 'uk.dev.io', country: 'uk', timeZone: 'Europe/Lodon' },
  { domain: 'fr.dev.io', country: 'fr', timeZone: 'Europe/Paris' },
  { domain: 'dubai.dev.io', country: 'ae', timeZone: 'Asia/Dubai' }
]
```
when the argument is `qa` (line 19 in the index.js file: loadDomains('qa')), output:
```
[
  { domain: 'china.qa.io', country: 'cn', timeZone: 'Asia/Shanghai' },
  { domain: 'jp.qa.io', country: 'jp', timeZone: 'Asia/tokyo' },
  { domain: 'uk.qa.io', country: 'uk', timeZone: 'Europe/Lodon' },
  { domain: 'fr.qa.io', country: 'fr', timeZone: 'Europe/Paris' },
  { domain: 'dubai.qa.io', country: 'ae', timeZone: 'Asia/Dubai' }
]
```

